<?php

use yii\db\Migration;

/**
 * Class m180111_112018_add_user
 */
class m180111_112018_add_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $this->insert('user', [
            'username' => 'admin',
            'email' => 'admin@admin.com',
            'auth_key' => 'Wx9rlaCkmXi0mh_aUX4-KfQcPCmubtbk',
            'password_hash' => '$2y$13$kJq2H54HRPwIQp.Y7pt5gOVBlcEc0ihQuwViIk8ZV34e44J8M78Tu',
            'status' => 10,
            'created_at' => 1515669580,
            'updated_at' => 1515669580
        ]);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180111_112018_add_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180111_112018_add_user cannot be reverted.\n";

        return false;
    }
    */
}
