<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order`.
 */
class m180111_095407_create_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('order', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(255)->defaultValue(''),
            'last_name' => $this->string(255)->defaultValue(''),
            'email' => $this->string(255)->defaultValue(''),
            'phone' => $this->string(255)->defaultValue(''),
            'address' => $this->integer(11)->defaultValue(0),
            'status' => $this->smallInteger(1)->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('order');
    }
}
