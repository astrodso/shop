<?php
return [
    'id' => 'app-common-tests',
    'basePath' => dirname(__DIR__),
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=shop',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
    ],
];
