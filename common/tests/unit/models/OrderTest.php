<?php
namespace common\tests\models;
use common\models\Order;


class OrderTest extends \Codeception\Test\Unit
{
    /**
     * @var \common\tests\UnitTester
     */
    protected $order;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testGetProductCount()
    {

        $order = new Order();
        $this->assertInternalType('int', $order->getProductsCount());

    }

    public function testGetAmount()
    {

        $order = new Order();
        $this->assertInternalType('int', $order->getAmount());

    }
}