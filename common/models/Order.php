<?php

namespace common\models;

use Yii;
use common\models\OrderProduct;

/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $phone
 * @property int $address
 * @property int $status
 */
class Order extends \yii\db\ActiveRecord
{

    const SCENARIO_ORDER = 'order';
    const SCENARIO_CART = 'cart';

    public function scenarios()
    {
        return [
            self::SCENARIO_ORDER => ['first_name', 'last_name', 'email', 'phone'],
            self::SCENARIO_CART => [],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email', 'phone'], 'required'],
            [['address', 'status'], 'integer'],
            [['first_name', 'last_name', 'email', 'phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => Yii::t('app', 'Имя'),
            'last_name' => Yii::t('app', 'Фамилия'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Телефон'),
            'address' => 'Address',
            'status' => 'Status',
        ];
    }

    public function getProductsCount(){
        $count = 0;
        $model = OrderProduct::find()->where(['order_id' => $this->id])->all();
        foreach ($model as $item){
            $count += $item->count;
        }
        return $count;
    }

    public function getAmount(){
        $price = 0;
        $model = OrderProduct::find()->where(['order_id' => $this->id])->all();
        foreach ($model as $item){
            $price += $item->price;
        }

        return $price;
    }

    public function getOrderProducts(){
        return $this->hasMany(OrderProduct::className(), ['order_id' => 'id']);
    }
}
