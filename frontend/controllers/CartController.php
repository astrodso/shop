<?php
namespace frontend\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\OrderProduct;
use common\models\Order;

/**
 * Site controller
 */
class CartController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $order = Yii::$app->cart->getOrder();
        if(empty($order->orderProducts)){
            return $this->goHome();
        }
        return $this->render('index', ['orderProducts' => $order->orderProducts]);
    }

    public function actionDelete($id)
    {
        Yii::$app->cart->delete($id, 1);
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionPlus($id)
    {
        Yii::$app->cart->add($id, 1);
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionOrder()
    {
        $model = Yii::$app->cart->getOrder();
        $model->scenario = Order::SCENARIO_ORDER;
        
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $model->status = 1;
            $model->save();

            //work time
            if(date('G') >= 9 && date('G') <= 18){
                $this->sendEmail(Yii::$app->params['workTimeEmail'], $model->id);
            } else {
                $this->sendEmail(Yii::$app->params['notWorkTimeEmail'], $model->id);
            }

            return $this->redirect(['site/order-success']);

        }
        
        return $this->render('order', ['model' => $model]);
    }

    public function actionAdd()
    {
        $postData = Yii::$app->request->post();
        return json_encode([
            'success' => Yii::$app->cart->add($postData['product_id'], 1),
            'cartStatus' => Yii::$app->cart->status
        ]);
    }

    public function sendEmail($email, $orderId){
        Yii::$app->mailer->compose()
        ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->name . ' robot'])
        ->setTo($email)
        ->setSubject("Заказ №$orderId")
        ->setHtmlBody("Заказ №$orderId принят")
        ->send();
    }

}
