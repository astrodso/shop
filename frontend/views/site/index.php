<?php

/* @var $this yii\web\View */

use yii\bootstrap\Button;
use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Каталог';
?>
<div class="site-index">

    <?php foreach($model as $item):?>
        <div>
            <?= Html::img('/uploads/store/'.$item->getImage()->filePath, ['width' => 100])?>
            <?= $item->title?>
            <?=
            Button::widget([
                'label' => 'в корзину',
                'options' => [
                    'class' => 'btn-sm btn-success',
                    'style' => 'margin:5px',
                    'onclick' => '$.post("'.Yii::$app->urlManager->createUrl(["cart/add"]).'",
                        {
                            product_id: '.$item->id.'
                        },
                        function( data ) {
                            var obj = $.parseJSON(data);
                            $(".cart").find("a").html(obj.cartStatus)
                        }
                    )',
                ]
            ]);
            ?>
        </div>
    <?php endforeach;?>
    
</div>
