<?php
use yii\helpers\Html;
use yii\bootstrap\Button;
?>
<div class="site-index">
    <table border="1">
        <tr>
            <td></td>
            <td>Название</td>
            <td>Цена</td>
            <td>Кол-во</td>
            <td></td>
        </tr>

        <?php foreach ($orderProducts as $orderProduct):?>
            <tr>
                <td>
                    <?= Html::img('/uploads/store/'.$orderProduct->product[0]->getImage()->filePath, ['width' => 100])?>
                </td>
                <td><?= $orderProduct->product[0]->title?></td>
                <td><?= $orderProduct->price?></td>
                <td><?= $orderProduct->count?></td>
                <td>
                    <?= Html::a('+', ['cart/plus', 'id' => $orderProduct->product[0]->id], [
                        'class'=>'btn btn-success',
                        'title' => 'Добавть'
                        ]
                    ) ?>
                    <?= Html::a('x', ['cart/delete', 'id' => $orderProduct->product[0]->id], [
                        'class'=>'btn btn-danger',
                        'title' => 'Удалить'
                        ]
                    ) ?>
                </td>
            </tr>
        <?php endforeach;?>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><?=  Yii::$app->cart->getAmount();?></td>
            </tr>
    </table>
    <?= Html::a('Оформить', ['cart/order'], [
        'class'=>'btn btn-primary',
        'title' => 'Оформить'
        ]
    ) ?>
</div>